package ru.t1.panasyuk.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.ILoggerService;
import ru.t1.panasyuk.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EntityListener implements MessageListener {

    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        final TextMessage textMessage = (TextMessage) message;
        final String text = textMessage.getText();
        loggerService.log(text);
    }

}