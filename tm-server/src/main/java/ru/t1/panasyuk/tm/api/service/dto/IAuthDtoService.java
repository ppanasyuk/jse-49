package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.enumerated.Role;

public interface IAuthDtoService {

    UserDTO check(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable String token);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDTO session) throws Exception;

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @Nullable String email, @Nullable Role role) throws Exception;

}