package ru.t1.panasyuk.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    @Nullable
    private UserDTO user;

    public UserChangePasswordResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserChangePasswordResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}